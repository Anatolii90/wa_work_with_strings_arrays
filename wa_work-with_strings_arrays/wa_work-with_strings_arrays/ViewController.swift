//
//  ViewController.swift
//  wa_work-with_strings_arrays
//
//  Created by Anatolii on 4/6/19.
//  Copyright © 2019 Anatolii. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let string = " АнатолийДмитриевич2/"
        
        //создадим строку, выведем количество символов без учета пробела
        StringSymbolCount(string: string)
        //проверим отчество на окончание ИЧ,НА
        stringSuffix(string: string)
        //вывод слитно написанного имени с пробелом
        spaceInNameString(string: string)
        //вывести строку зеркально
        reverseString(string: string)
        //запятые в строку как в калькуляторе
        commaInString(string: string)
        //проверим пароль на надежность
        checkPasswordSafety(string: string)
        
        //блок работы с массивами
        let array = [0, 10, 3, 5, 3, 7, 9, 7, 4, 3]
        let str = "Привет fuck"
        //отсоритуруем чисельный массив
        sortArray(array: array)
        //перевод строки в транислит
        translit(str: str)
        //выыод всех строк где содержиться искомая подстрока
        let stringArray = ["lada", "sedan", "backlazhan"]
        let searchingStr = "da"
        searchSubSring(str: stringArray, searchingStr: searchingStr)
        //антимат
        antimat(str: str)
    }

    func StringSymbolCount(string: String) {
        print("Исходная строка: \(string)")
        print("Исходная строка имеет \(string.count) символов")
        print("-------------------------------------------------------")
    }
    
    func stringSuffix(string: String) {
        print("Исходная строка: \(string)")
        if string.hasSuffix("ич") {
            print("ты мужчина")
        }
        else if string.hasSuffix("на") {
            print("ты женщина")
        }
        else {
            print("ты какое то безродное существо")
        }
        print("-------------------------------------------------------")
    }
    
    func spaceInNameString(string: String) {
        var finalString = string
        for index in string.indices {
            let currSymbol = String(string[index])
            let currSymbolUppercased = currSymbol.uppercased()
            if currSymbol == currSymbolUppercased
                && index != string.startIndex {
                finalString.insert(" ", at:index)
            }
        }
        print("Исходная строка: \(string)")
        print("Исходная строка с учетом пробелов: \(finalString)")
        print("-------------------------------------------------------")
    }
    
    func reverseString(string: String) {
        var finalString = ""
        var count = string.count-1
        while count >= 0 {
            finalString+=String(string[string.index(string.startIndex, offsetBy: count)])
            count-=1
        }
        print("Исходная строка: \(string)")
        print("Исходная строка задом наперед: \(finalString)")
        print("-------------------------------------------------------")
    }
    
    func commaInString(string: String) {
        var finalString = string
        var numberOfCommas = Int(string.count/3)
        if Int(string.count) % 3 == 0 {
            numberOfCommas-=1
        }
        var index = string.count-3
        for _ in 0..<numberOfCommas {
            finalString.insert(",", at: finalString.index(finalString.startIndex, offsetBy: index))
            index-=3
        }
        print(finalString)
    }
    
    func checkPasswordSafety(string: String) {
        var str = string
        let numericSet = CharacterSet.decimalDigits
        let symbolSet = CharacterSet.punctuationCharacters
        let uppercaseSet = CharacterSet.uppercaseLetters
        let lowercaseSet = CharacterSet.lowercaseLetters
        var passHardness = 0
        if string.rangeOfCharacter(from: numericSet) != nil {
            passHardness += 1
        }
        if string.rangeOfCharacter(from: symbolSet) != nil {
            passHardness+=1
        }
        if string.rangeOfCharacter(from: uppercaseSet) != nil {
            passHardness+=1
        }
        if string.rangeOfCharacter(from: lowercaseSet) != nil {
            passHardness+=1
        }
        print("Исходная строка: \(string)")
        print("уровень защиты данного пароля: \(passHardness)")
        print("-------------------------------------------------------")
    }
    
    func sortArray(array: [Int]) {
        var finalArray = array
        let count = finalArray.count
        for _ in 0..<count {
            for i in 0..<count-1 {
                if i < finalArray.count-1 {
                    if finalArray[i+1] < finalArray[i] {
                        let currValue = finalArray[i]
                        finalArray[i] = finalArray[i+1]
                        finalArray[i+1] = currValue
                    }
                    else if finalArray[i+1] == finalArray[i] {
                        finalArray.remove(at: i+1)
                    }
                }
            }
        }
        print("исходный массив: \(array)")
        print("отсортированный массив: \(finalArray)")
        print("-----------------------------------------------")
    }
    
    func translit(str: String) {
        let translitBook = ["П": "P",
                            "п": "p",
                            "р": "r",
                            "и": "i",
                            "в": "v",
                            "е": "e",
                            "т": "t"]
        var newStr = ""
        for i in str.indices {
            let currSymbol = String(str[i])
            let unwrappedCurrSymbol = translitBook[currSymbol] ?? ""
            newStr+=unwrappedCurrSymbol
        }
        print("исходная строка: \(str)")
        print("транслитная строка: \(newStr)")
        print("-----------------------------------------------")
    }
    
    func  searchSubSring(str: [String], searchingStr: String) {
        var resultArray = [] as [String]
        for i in 0..<str.count {
            let predicate = NSPredicate(format: "SELF contains %@", searchingStr)
            let result = str[i].filter{_ in predicate.evaluate(with: str[i])}
            if !result.isEmpty {
                resultArray.append(result)
            }
        }
        print("исходый массив: \(str)")
        print("строка вхождение которой мы ищем в элементах массива: \(searchingStr)")
        print("искомые элементы массива: \(resultArray)")
        print("-----------------------------------------------")
    }
    
    func antimat(str: String) {
        var incomeString = str
        var finalString = ""
        let badWords: Set = ["fuck", "fak"]
        while incomeString != "" {
            var tempStarsString = ""
            let probel = incomeString.firstIndex(of: " ") ?? incomeString.endIndex
            let firstWord = incomeString[..<probel]
            if badWords.contains(String(firstWord)) {
                for _ in 0..<firstWord.count {
                    tempStarsString.append("*")
                }
            }
            else {
                tempStarsString = String(firstWord)
            }
            finalString.append(tempStarsString)
            if probel != incomeString.endIndex {
                incomeString = String(incomeString.suffix(from: incomeString.index(after: probel)))
            }
            else {
                incomeString = ""
            }
        }
        print("исходная строка: \(str)")
        print("антимат: \(finalString)")
        print("-----------------------------------------------")
    }

}

